import sys


MOVE_CMDS = {
	'north': (0,-1),
	'south': (0,1),
	'east': (1,0),
	'west': (-1,0)
}
ACTION_CMDS = {
	'alpha': ((-1, -1), (-1, 1), (1, -1), (1, 1)),
	'beta': ((-1, 0), (0, -1), (0, 1), (1, 0)),
	'gamma': ((-1, 0), (0, 0), (1, 0)),
	'delta': ((0, -1), (0, 0), (0, 1))
}

def input_file_reader(path):
	'''readers file from the path'''
	return open(path, 'r')

def get_script_cmds(script):
	'''reads script commands from script file'''
	script_cmds = list()

	line = script.readline()
	while line:
		script_cmds.append(line.strip('\n').split(" "))
		line = script.readline()

	return script_cmds

def get_field_grid(field):
	'''reads input field from field file'''
	field_grid = list()

	line = field.readline()
	while line:
		field_grid.append(list(line.strip('\n')))
		line = field.readline()

	return field_grid

def get_mine_positions(grid):
	'''gives mines positions'''
	mine_positions = []

	for row_index in range(len(grid)):
		for column_index in range(len(grid[row_index])):
			if  grid[row_index][column_index]!=".":
				mine_positions.append((column_index, row_index))

	return mine_positions

def get_mine_values(grid):
	'''give mines values'''
	global mine_positions
	mine_values = []

	for position in mine_positions:
		mine_values.append(grid[position[1]][position[0]])

	return mine_values

def get_fire_positions(command, vessel_position):
	'''give fire positions'''
	fire_positions = []

	for positions in ACTION_CMDS[command]:
		fire_positions.append(tuple(x+y for x, y in zip(vessel_position, positions)))

	return fire_positions

def perform_fire(fire_positions, grid):
	'''performs the fire operation'''
	for position in fire_positions:
		try:
			grid[position[1]][position[0]] = '.'
		except IndexError:
			pass

	return grid

def perform_move(command):
	'''performs the move operation'''
	global vessel_position
	print("vesel before move {}".format(vessel_position))
	new_vessel_position = list(x+y for x, y in zip(vessel_position, MOVE_CMDS[command]))
	print("vesel after move {}".format(new_vessel_position))
	return new_vessel_position

def calculate_new_grid(mine_positions):
	'''calculates the new field grid'''
	print(mine_positions)
	global vessel_position

	horizontal_size = vessel_position[0]
	vertical_size = vessel_position[1]

	for mine in mine_positions:
		if abs(mine[0])>horizontal_size:
			horizontal_size = abs(mine[0]-horizontal_size)
		if abs(mine[1])>vertical_size:
			vertical_size = abs(mine[1]-vertical_size)

	vessel_position[0] = horizontal_size
	vessel_position[1] = vertical_size

	new_grid = []

	for row in range(2*vertical_size+1):
		new_grid.append(list('.'*(2*horizontal_size+1)))
	
	return new_grid

def calculate_relative_position_of_mines(vessel_position, mine_positions):
	'''calculates the relative position of the mines from the vessel'''
	relative_positions = []

	for position in mine_positions:
		relative_positions.append(tuple(x-y for x, y in zip(position, vessel_position)))

	print("relative mine positions {}".format(relative_positions))
	return relative_positions

def map_mines(relative_positions, vessel_position, grid):
	'''map mines in the new field'''
	global mine_values
	new_mine_positions = []
	for position in relative_positions:
		new_mine_positions.append(tuple(x+y for x, y in zip(position, vessel_position)))

	for position, mine_value in zip(new_mine_positions, mine_values):
		if ord(mine_value)<98:
			grid[position[1]][position[0]] = '*'
		else:
			grid[position[1]][position[0]] = chr(ord(mine_value)-1)
	
	return grid

def process_move(command, grid):
	'''process any move'''
	global vessel_position
	global mine_positions
	global mine_values

	vessel_position = perform_move(command)
	relative_positions = calculate_relative_position_of_mines(vessel_position, mine_positions)
	grid = calculate_new_grid(mine_positions)
	new_vessel_position = [int(len(grid[0])/2), int(len(grid)/2)]
	grid = map_mines(relative_positions, new_vessel_position, grid)
	mine_positions = get_mine_positions(grid)
	mine_values = get_mine_values(grid)
	return grid

def has_mines(mine_positions):
	'''checks if any mines available'''
	if len(mine_positions)>0:
		return True

	return False

def mine_miss_check(mine_values):
	'''checks if any mine is missed'''
	return any([mine_value=='*' for mine_value in mine_values])

def move(command, grid):
	'''move command function'''
	global vessel_position
	global mine_positions
	global mine_values

	if command == 'north':
		grid = process_move(command, grid)

		return grid

	elif command == 'south':
		grid = process_move(command, grid)

		return grid

	elif command == 'east':
		grid = process_move(command, grid)

		return grid

	else:
		grid = process_move(command, grid)

		return grid

def action(command, grid):
	'''action command function'''
	global vessel_position
	global mine_positions

	if command == 'alpha':
		fire_positions = get_fire_positions(command, vessel_position)
		grid = perform_fire(fire_positions, grid)
		mine_positions = get_mine_positions(grid)

		return grid

	elif command == 'beta':
		fire_positions = get_fire_positions(command, vessel_position)
		grid = perform_fire(fire_positions, grid)
		mine_positions = get_mine_positions(grid)

		return grid

	elif command == 'gamma':
		fire_positions = get_fire_positions(command, vessel_position)
		grid = perform_fire(fire_positions, grid)
		mine_positions = get_mine_positions(grid)

		return grid

	else:
		fire_positions = get_fire_positions(command, vessel_position)
		grid = perform_fire(fire_positions, grid)
		mine_positions = get_mine_positions(grid)

		return grid

def output_writer_1(grid, step_count, script_cmds):
	'''writes output before any STEP'''
	with open("test.result",'a',encoding = 'utf-8') as output_file:
		output_file.write("Step {}".format(step_count))
		output_file.write('\n'*2)

		for line in grid:
			output_file.write("".join(line))
			output_file.write('\n')

		output_file.write('\n')
		output_file.write(" ".join(script_cmds))
		output_file.write('\n'*2)

def output_writer_2(grid):
	'''writes output after any STEP'''
	with open("test.result",'a',encoding = 'utf-8') as output_file:

		if has_mines(mine_positions):
			for line in grid:
				output_file.write("".join(line))
				output_file.write('\n')
			output_file.write('\n')

#writes the final SCORE
def score_writer(no_of_move_perfomed, no_of_action_perfomed, score,initial_no_of_mines, step_count):
	with open("test.result",'a',encoding = 'utf-8') as output_file:

		if len(mine_positions)>0:
			output_file.write('fail(0)')

		else:
			if len(mine_positions)==0 and step_count==len(script_cmds):
				action_score = 5*no_of_action_perfomed
				move_score = 2*no_of_move_perfomed
				if action_score>5*initial_no_of_mines:
					action_score = 5*initial_no_of_mines
				if move_score>3*initial_no_of_mines:
					move_score = 3*initial_no_of_mines
				score = score - (action_score + move_score)

				output_file.write('.\n')
				output_file.write('\npass ({})'.format(score))

			else:
				output_file.write('.\n')
				output_file.write('\npass (1)')


if __name__ == '__main__':
	field_file = input_file_reader(sys.argv[1])
	script_file = input_file_reader(sys.argv[2])

	script_cmds = get_script_cmds(script_file)
	field_grid = get_field_grid(field_file)
	
	mine_positions = get_mine_positions(field_grid)

	mine_values = get_mine_values(field_grid)
	vessel_position = [int(len(field_grid[0])/2), int(len(field_grid)/2)]

	initial_no_of_mines = len(mine_positions)

	score = initial_no_of_mines*10
	step_count = 0
	no_of_action_perfomed = 0
	no_of_move_perfomed = 0
	break_flag = False
	
	for cmds in script_cmds:
		if break_flag:
			break
		if has_mines(mine_positions):
			step_count +=1
			output_writer_1(field_grid, step_count, cmds)
	
			for cmd in cmds:
				if cmd in MOVE_CMDS:
					no_of_move_perfomed += 1 
					field_grid = move(cmd, field_grid)
					if mine_miss_check(mine_values):
						break_flag = True
				else:
					no_of_action_perfomed += 1
					field_grid = action(cmd, field_grid)

		output_writer_2(field_grid)

	score_writer(no_of_move_perfomed, no_of_action_perfomed, score, initial_no_of_mines, step_count)